const path = require('path');
function resolve(dir){
    return path.resolve(__dirname,dir)
}
module.exports = {
    configureWebpack:{
        resolve: {
              alias: {
                'src': resolve('src'),
                  'assets': resolve('src/assets'),
                  '@': resolve('src')
              }
            }
      }
  }